import store from '../store'
import _ from 'lodash'
import React from 'react'
import {Link} from 'react-router-dom'
import {ContractsService} from './contracts-service'

export class Listing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contracts: []
    }
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    this.setState({
      contracts: await ContractsService.getContracts()
    })
  }

  getMatchingContracts() {
    console.log(store.getState())
    return _.filter(this.state.contracts, c => JSON.stringify(c).toLowerCase().includes(store.getState().search.toLowerCase()))
  }

  render() {
    return (
      <div>
        <h2>Contracts</h2>

        <Link to="/contracts/new" className="btn btn-default">New Contract</Link>

        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody>
            {this.getMatchingContracts().map(r => (
                <tr key={r.id}>
                  <td>
                    <Link to={`/contracts/${r.id}`}>
                      {r.name}
                    </Link>
                  </td>
                  <td>{r.price}</td>
                  <td>{r.note}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    )
  }
}
