export const ROUTES = {
  CONTACTS_NEW: '/contacts/new',
  CONTACTS_EDIT: '/contacts/:id/edit',
  CONTACTS_SHOW: '/contacts/:id',
  CONTACTS_LISTING: '/contacts',

  CONTRACTS_NEW: '/contracts/new',
  CONTRACTS_EDIT: '/contracts/:id/edit',
  CONTRACTS_SHOW: '/contracts/:id',
  CONTRACTS_LISTING: '/contracts',
}
