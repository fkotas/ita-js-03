import store from './store'

export class AuthService {
  static login() {
    store.dispatch({type: 'LOGIN'})
  }

  static isLoggedIn() {
    return store.getState().loggedIn
  }

  static logout() {
    store.dispatch({type: 'LOGOUT'})
  }
}
