import _ from 'lodash'
import {createStore} from 'redux'

const initialState = {
  search: '',
  loggedIn: false
}

const store = createStore((state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return _.assign({}, state, {loggedIn: true})

    case 'LOGOUT':
      return _.assign({}, state, {loggedIn: false})

    case 'SEARCH':
      return _.assign({}, state, {search: action.search})

    default:
      return state
  }
})

console.log(store.getState())

export default store
