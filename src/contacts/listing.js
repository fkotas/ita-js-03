import store from '../store'
import _ from 'lodash'
import React from 'react'
import {Link} from 'react-router-dom'
import {ContactsService} from './contacts-service'

export class Listing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contacts: [],
      search: ''
    }
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    this.setState({
      contacts: await ContactsService.getContacts()
    })
  }

  getMatchingContacts() {
    return _.filter(this.state.contacts, c => JSON.stringify(c).toLowerCase().includes(store.getState().search.toLowerCase()))
  }

  render() {
    return (
      <div>
        <h2>Contacts</h2>

        <Link to="/contacts/new" className="btn btn-default">New Contact</Link>

        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone</th>
              <th>Address</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody>
            {this.getMatchingContacts().map(r => (
                <tr key={r.id}>
                  <td>
                    <Link to={`/contacts/${r.id}`}>
                      {r.name}
                    </Link>
                  </td>
                  <td>{r.phone}</td>
                  <td>{r.address}</td>
                  <td>{r.note}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    )
  }
}
