import React from 'react'
import { ContactsService } from './contacts-service'
import {Form} from './form'

export class New extends React.Component {
  constructor() {
    super()

    this.state = {
      contact: {
        name: 'New',
        phone: '',
        address: '',
        note: ''
      }
    }
  }

  async create() {
    await ContactsService.create(this.state.contact)
    this.props.history.push('/contacts')
  }

  render() {
    return (
      <div>
        <h2>Edit Contact</h2>

        <Form contact={this.state.contact} />

        <div>
          <button className="btn btn-default" onClick={this.create.bind(this)}>Create</button>
        </div>
      </div>
    )
  }
}
