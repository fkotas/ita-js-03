import React from 'react'
import {ContactsService} from '../contacts/contacts-service'

export class Form extends React.Component {
  constructor() {
    super()
    this.state = {
      contacts: []
    }
  }

  componentWillMount() {
    this.loadContacts()
  }

  async loadContacts() {
    this.setState({
      contacts: await ContactsService.getContacts()
    })
  }

  onChange(e) {
    this.props.contract[e.target.name] = e.target.value
    this.forceUpdate()
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-6">
          <div className="form-group">
            <label>Name</label>
            <input type="text" className="form-control" name="name" value={this.props.contract.name} onChange={this.onChange.bind(this)} />
          </div>

          <div className="form-group">
            <label>Contact</label>
            <select className="form-control" name="contact_id" value={this.props.contract.contact_id} onChange={this.onChange.bind(this)}>
              {this.state.contacts.map(c => <option key={c.id} value={c.id}>{c.name}</option>)}
            </select>
          </div>

          <div className="form-group">
            <label>Price</label>
            <input type="text" className="form-control" name="price" value={this.props.contract.price} onChange={this.onChange.bind(this)} />
          </div>
        </div>
        <div className="col-md-6">
          <div className="form-group">
            <label>Note</label>
            <textarea className="form-control" name="note" value={this.props.contract.note} onChange={this.onChange.bind(this)}></textarea>
          </div>
        </div>
      </div>
    )
  }
}
