import store from './store'
import React, {Component} from 'react';
import {AuthService} from './auth-service'

import {ROUTES} from './routes'
import {Nav} from './nav'
import * as contactScreens from './contacts'
import * as contractScreens from './contracts'
import {Login} from './login'

import {Route, HashRouter as Router, Switch, Redirect} from 'react-router-dom'

class App extends Component {
  componentWillMount() {
    this.unsubcribe = store.subscribe(e => this.forceUpdate())
  }

  componentWillUnmount() {
    this.unsubcribe()
  }

  render() {
    return (
      <Router>
        <div>
          <Nav />

          <div className="container">
            {
              (AuthService.isLoggedIn() &&
              <Switch>
                <Route path={ROUTES.CONTACTS_NEW} component={contactScreens.New} />
                <Route path={ROUTES.CONTACTS_EDIT} component={contactScreens.Edit} />
                <Route path={ROUTES.CONTACTS_SHOW} component={contactScreens.Show} />
                <Route path={ROUTES.CONTACTS_LISTING} component={contactScreens.Listing} />

                <Route path={ROUTES.CONTRACTS_NEW} component={contractScreens.New} />
                <Route path={ROUTES.CONTRACTS_EDIT} component={contractScreens.Edit} />
                <Route path={ROUTES.CONTRACTS_SHOW} component={contractScreens.Show} />
                <Route path={ROUTES.CONTRACTS_LISTING} component={contractScreens.Listing} />

                <Redirect from="/" to="/contacts" />
              </Switch>) || <Login />
            }
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
