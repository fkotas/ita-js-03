import {ROUTES} from './routes'
import React from 'react'
import {Link} from 'react-router-dom'
import store from './store'
import {AuthService} from './auth-service'

export class Nav extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-default">
        <div className="container">
          <div className="navbar-header">
            <button type="button" className="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2"
              aria-expanded="false">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <Link to="/" className="navbar-brand">App</Link>
          </div>
          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul className="nav navbar-nav">
              <li>
                <Link to={ROUTES.CONTACTS_LISTING}>Contacts</Link>
              </li>
              <li>
                <Link to={ROUTES.CONTRACTS_LISTING}>Contracts</Link>
              </li>
            </ul>

            <div className="navbar-right">
              <a className="navbar-text" onClick={e => AuthService.logout()}>Logout</a>
            </div>

            <div className="navbar-form navbar-right">
              <div className="form-group">
                <input type="text" className="form-control" placeholder="Search..." value={store.getState().search} onChange={(e) => {store.dispatch({type: 'SEARCH', search: e.target.value})}} />
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}
