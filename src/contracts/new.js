import React from 'react'
import { ContractsService } from './contracts-service'
import {Form} from './form'

export class New extends React.Component {
  constructor() {
    super()

    this.state = {
      contract: {
        name: 'New',
        price: 0,
        note: ''
      }
    }
  }

  async create() {
    await ContractsService.create(this.state.contract)
    this.props.history.push('/contracts')
  }

  render() {
    return (
      <div>
        <h2>Edit Contract</h2>

        <Form contract={this.state.contract} />

        <div>
          <button className="btn btn-default" onClick={this.create.bind(this)}>Create</button>
        </div>
      </div>
    )
  }
}
