import React from 'react'
import {Link} from 'react-router-dom'
import {ContactsService} from './contacts-service'

export class Show extends React.Component {
  constructor(props){
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    this.load(nextProps.match.params.id)
  }

  async load(id) {
    this.setState({contact: await ContactsService.getContact(id)})
  }

  render() {
    return (
      <div>
        <h2>Contact detail</h2>

        {this.state.contact && <div>
          <div className="row">
            <div className="col-sm-6">
              <div className="row">
                <div className="col-sm-4">Name</div>
                <div className="col-sm-8">{this.state.contact.name}</div>
              </div>

              <div className="row">
                <div className="col-sm-4">Phone</div>
                <div className="col-sm-8">{this.state.contact.phone}</div>
              </div>

              <div className="row">
                <div className="col-sm-4">Address</div>
                <div className="col-sm-8">{this.state.contact.address}</div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="row">
                <div className="col-sm-4">Note</div>
                <div className="col-sm-8">{this.state.contact.note}</div>
              </div>
            </div>
          </div>

          <div>
            <Link to={`/contacts/${this.state.contact.id}/edit`} className="btn btn-default">Edit</Link>
            <button className="btn btn-default">Delete</button>
          </div>
        </div>}
      </div>
    )
  }
}
