import React from 'react'
import { ContractsService } from './contracts-service'
import {Form} from './form'

export class Edit extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    this.load(nextProps.match.params.id)
  }

  async load(id) {
    this.setState({contract: await ContractsService.getContract(id)})
  }

  async update() {
    await ContractsService.update(this.state.contract)
    this.props.history.push('/contracts')
  }

  render() {
    return (
      <div>
        <h2>Edit Contract</h2>

        {this.state.contract && <div>
          <Form contract={this.state.contract} />

          <div>
            <button className="btn btn-default" onClick={this.update.bind(this)}>Update</button>
          </div>
        </div>}
      </div>
    )
  }
}
