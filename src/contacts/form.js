import React from 'react'

export class Form extends React.Component {
  onChange(e) {
    this.props.contact[e.target.name] = e.target.value
    this.forceUpdate()
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-6">
          <div className="form-group">
            <label>Name</label>
            <input type="text" className="form-control" name="name" value={this.props.contact.name} onChange={this.onChange.bind(this)} />
          </div>

          <div className="form-group">
            <label>Phone</label>
            <input type="text" className="form-control" name="phone" value={this.props.contact.phone} onChange={this.onChange.bind(this)} />
          </div>

          <div className="form-group">
            <label>Address</label>
            <textarea className="form-control" name="address" value={this.props.contact.address} onChange={this.onChange.bind(this)}></textarea>
          </div>
        </div>
        <div className="col-md-6">
          <div className="form-group">
            <label>Note</label>
            <textarea className="form-control" name="note" value={this.props.contact.note} onChange={this.onChange.bind(this)}></textarea>
          </div>
        </div>
      </div>
    )
  }
}
