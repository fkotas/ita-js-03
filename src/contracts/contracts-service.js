import _ from 'lodash'
import $http from 'axios'

export class ContractsService {
  static async getContracts() {
    return (await $http.get('http://localhost:1234/contracts')).data
  }

  static async getContract(id) {
    return (await $http.get(`http://localhost:1234/contracts/${id}`)).data
  }

  static async save(contract) {
    if (contract.id) {
      return this.update(contract)
    }

    return this.create(contract)
  }

  static async update(contract) {
    return (await $http.post(`http://localhost:1234/contracts/${contract.id}`, contract)).data
  }

  static async create(data) {
    return (await $http.post(`http://localhost:1234/contracts`, data))
  }
}
