import _ from 'lodash'
import bodyParser from 'body-parser'
import express from 'express'
import fs from 'fs'
import knex from 'knex'
import {Model} from 'objection'

const api = express()

const db = knex({
  client: 'pg',
  connection: {
    user: 'cztomsik',
    database: 'contacts'
  }
})

Model.knex(db)

class Base extends Model {
  static get tableName() {
    return this.name.toLocaleLowerCase()
  }

  static async findById(id) {
    return await this.query().where({id: id}).limit(1).first();
  }
}

class Contact extends Base {

}

class Contract extends Base {

}

api.use(bodyParser.json())

api.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

api.get('/contacts', async (req, res) => {
  res.send(await Contact.query())
})

api.post('/contacts', async (req, res) => {
  await Contact.query().insert(req.body)
  res.status(204).end()
})

api.get('/contacts/:id', async (req, res) => {
  res.send(await Contact.findById(req.params.id))
})

api.post('/contacts/:id', async (req, res) => {
  const c = await Contact.findById(req.params.id)

  await c.$query().update(req.body)
  res.status(204).end()
})

api.delete('/contacts/:id', async (req, res) => {
  const c = await Contact.findById(req.params.id)

  await c.$query().delete()
  res.status(204).end()
})

api.get('/contracts', async (req, res) => {
  res.send(await Contract.query())
})

api.post('/contracts', async (req, res) => {
  await Contract.query().insert(req.body)
  res.status(204).end()
})

api.get('/contracts/:id', async (req, res) => {
  res.send(await Contract.findById(req.params.id))
})

api.post('/contracts/:id', async (req, res) => {
  const c = await Contract.findById(req.params.id)

  await c.$query().update(req.body)
  res.status(204).end()
})

api.delete('/contracts/:id', async (req, res) => {
  const c = await Contract.findById(req.params.id)

  await c.$query().delete()
  res.status(204).end()
})

api.listen(1234)
