import React from 'react'
import {AuthService} from './auth-service'

export class Login extends React.Component {
  render() {
    return (
      <div>
        <h2>Login</h2>

        <label htmlFor="">Login</label>
        <input type="text" className="form-control"/>

        <label htmlFor="">Password</label>
        <input type="text" className="form-control"/>

        <button className="btn btn-primary" onClick={e => AuthService.login()}>Login</button>
      </div>
    )
  }
}
