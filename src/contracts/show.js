import React from 'react'
import {Link} from 'react-router-dom'
import {ContractsService} from './contracts-service'

export class Show extends React.Component {
  constructor(props){
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    this.load(nextProps.match.params.id)
  }

  async load(id) {
    this.setState({contract: await ContractsService.getContract(id)})
  }

  render() {
    return (
      <div>
        <h2>Contract detail</h2>

        {this.state.contract && <div>
          <div className="row">
            <div className="col-sm-6">
              <div className="row">
                <div className="col-sm-4">Name</div>
                <div className="col-sm-8">{this.state.contract.name}</div>
              </div>

              <div className="row">
                <div className="col-sm-4">Price</div>
                <div className="col-sm-8">{this.state.contract.price}</div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="row">
                <div className="col-sm-4">Note</div>
                <div className="col-sm-8">{this.state.contract.note}</div>
              </div>
            </div>
          </div>

          <div>
            <Link to={`/contracts/${this.state.contract.id}/edit`} className="btn btn-default">Edit</Link>
            <button className="btn btn-default">Delete</button>
          </div>
        </div>}
      </div>
    )
  }
}
