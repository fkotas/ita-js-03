import React from 'react'
import { ContactsService } from './contacts-service'
import {Form} from './form'

export class Edit extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    this.load(nextProps.match.params.id)
  }

  async load(id) {
    this.setState({contact: await ContactsService.getContact(id)})
  }

  async update() {
    await ContactsService.update(this.state.contact)
    this.props.history.push('/contacts')
  }

  render() {
    return (
      <div>
        <h2>Edit Contact</h2>

        {this.state.contact && <div>
          <Form contact={this.state.contact} />

          <div>
            <button className="btn btn-default" onClick={this.update.bind(this)}>Update</button>
          </div>
        </div>}
      </div>
    )
  }
}
